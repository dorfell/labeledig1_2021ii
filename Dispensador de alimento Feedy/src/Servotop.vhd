----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.01.2022 12:19:02
-- Design Name: 
-- Module Name: Servotop - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Servotop is
 PORT(  clk  : IN  STD_LOGIC;
        reset: IN  STD_LOGIC;
        pos  : IN  STD_LOGIC_VECTOR(7 downto 0);
        servo: OUT STD_LOGIC
    );
end Servotop;

architecture Behavioral of Servotop is

    component   PWM
        PORT( clk, reset: in std_logic;
            clk_out: out std_logic);
    END COMPONENT;
    
    component servo_pwm
   Port ( clk, rst: in std_logic;
        pos1   : IN  STD_LOGIC_VECTOR(7 downto 0);
        servo : OUT STD_LOGIC);
   end component;
   
    signal clk_out : STD_LOGIC := '0';
    
begin
    clk50z_map: PWM PORT MAP(
       clk, reset, clk_out     
    );

   servo_pwm_map: servo_pwm PORT MAP(
        clk_out, reset, pos, servo
    );
end Behavioral;

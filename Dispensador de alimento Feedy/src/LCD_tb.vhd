library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LCD_tb is
  --Port ();

end LCD_tb;

architecture Behavioral of LCD_tb is

component LIB_LCD_INTESC_REVD
port( CLK: IN STD_LOGIC;
      RS 		  : OUT STD_LOGIC;							--
	  RW		  : OUT STD_LOGIC;							--
	  ENA 	  : OUT STD_LOGIC;							--
	  DATA_LCD : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    reset : in std_logic;
    rx : in std_logic;
    btn_wr:in std_logic;
    tx : out std_logic;
    tx_full: out std_logic;
        bien: out std_logic;
        flag_5s: out std_logic;
        PWM: out std_logic;
        ECO: in std_logic;
        TRIGGER      : OUT STD_LOGIC;
        Parlante: out std_logic
);
end component;

signal clk: std_logic := '0';
signal RS: std_logic := '0';
signal RW: std_logic := '0';
signal ENA: std_logic := '0';
signal DATA_LCD: STD_LOGIC_VECTOR(7 DOWNTO 0):= (others=>'0');
signal    reset : std_logic := '0';
signal    rx : std_logic;
signal    btn_wr:std_logic;
signal    tx : std_logic;
signal    tx_full: std_logic;
signal        bien: std_logic:= '0';
signal        flag_5s: std_logic:= '0';
signal        PWM: std_logic:= '0';
signal        ECO: std_logic:= '0';
signal        TRIGGER    :  STD_LOGIC:= '0';
signal        Parlante: std_logic:= '0';

begin

uut: LIB_LCD_INTESC_REVD
port map (
RS => RS,
RW => RW,
ENA=> ENA,
DATA_LCD=>DATA_LCD,
clk => clk,
reset =>reset,
rx => rx,
btn_wr => btn_wr,
tx => tx,
tx_full=>tx_full,
bien=>bien,
flag_5s=>flag_5s,
PWM=>PWM,
ECO=>ECO,
TRIGGER=>TRIGGER,
Parlante=>Parlante
);

--Stimulus process
stim_proc: process
begin
wait for 60 ms;

rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
  
  
--------------------------------------------(tramas basura)

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
 ---------------------------------------------------- 
  
   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

----------------------------------------------------------------------------CONTRASE?A correcta
 wait for 30 ms;
 
   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='0'; ---cambiao
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
    

  
  --------------------------------------------------------
 
   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
  
--------------------------------------------------

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

-------------------------------------------------------------

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

----------------------------------------- TRAMAS BASURA

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
 ---------------------------------------------------- 
  
    rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

------------------------------------------------------------------------Envia C activa cambiar hora
    wait for 10 ms;

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

----------------------------------------- TRAMAS BASURA

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
 ---------------------------------------------------- 
  
    rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

------------------------------------------------------------------------------- Env?a nueva hora

wait for 30 ms;

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
  
--------------------------------------------------

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

-------------------------------------------------------------

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;

       rx<='1';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;

----------------------------------------- TRAMAS BASURA

   rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;
 ---------------------------------------------------- 
  
    rx<='0'; --Start bit
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;
  
       rx<='1';
  wait for 104.2 us;
  
       rx<='0';
  wait for 104.2 us;

       rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='0';
  wait for 104.2 us;
  
     rx<='1'; --Stop bit
  wait for 104.2 us;



    -------------------------------------------------------- 
    --------------------------------------------------------
    
--    w_data<="00111000";
--    wait for 104.2 us;
    
--     btn_wr<='1';
--  wait for 1 ms;
  
--         btn_wr<='0';
--  wait for 104.2 us;
  
  
--   w_data<="00110001";
--    wait for 104.2 us;
    
--     btn_wr<='1';
--  wait for 1 ms;
  
--         btn_wr<='0';
--  wait for 104.2 us;
    
  
wait;
end process;

clk <= not clk after 4 ns;

end Behavioral;
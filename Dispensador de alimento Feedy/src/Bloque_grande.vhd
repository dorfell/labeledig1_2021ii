----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bloque_grande is
  Port (clk, reset : in std_logic;
        btn_wr:in std_logic;
        rx: in std_logic;
        rd_uart: in std_logic;
        w_data: in std_logic_vector (7 downto 0);
        r_data: out std_logic_vector (7 downto 0);
        rx_empty: out std_logic;
        tx_full: out std_logic;
        tx: out std_logic
   );
end Bloque_grande;

architecture Behavioral of Bloque_grande is

signal wr_uart: std_logic:='0';

begin

uart_unit: entity work.uart(str_arch)
           port map (clk=>clk, reset=>reset,
           rd_uart=>rd_uart, wr_uart=>wr_uart,
           rx=>rx,w_data=>w_data,
           tx_full=>tx_full, rx_empty=>rx_empty,
           tx=>tx, r_data=>r_data );
             
bloque_tx_unit: entity work.bloque_tx(arch)
           port map ( clk=>clk,reset=>reset,btn_wr=>btn_wr,
             wr_bloque=>wr_uart);

end Behavioral;
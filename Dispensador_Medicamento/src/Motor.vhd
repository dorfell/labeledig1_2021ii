library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Motor is
   Port (clk,rst,en,moto: in std_logic;
   segmento: in STD_LOGIC_VECTOR (3 downto 0);
            smotor: out std_logic_vector(3 downto 0) );
end Motor;

architecture Behavioral of Motor is

 type states is (s00,s0,s1,s2,s3,s4,s5,s6,s7);
    signal present_state, next_state: states;
signal smotor1: std_logic_vector(3 downto 0):=(others=>'0'); 
begin
   process(clk, rst)
  begin
  if rst = '1' then
    present_state <= s00;
  elsif (clk'event and clk='1') then
    if en='1' then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
	 end if;
  end if;
 end process;

process(present_state, moto,segmento)
  begin 
  case present_state is 
    when s00 =>
	   if (moto = '1' or segmento="1001" ) then
		   next_state <= s0;
		else 
		   next_state <= s00;
		end if;
  
    when s0 =>
		   next_state <= s1;

    when s1 =>
	       next_state <= s2;

	    

    when s2 =>
	       next_state <= s3;

    when s3 =>
	       next_state <= s4;

	   
	when s4 =>
	       next_state <= s5;
	
	when s5 =>
	       next_state <= s6;

	   
	when s6 =>

	       next_state <= s7;
	   
	when s7 =>
	       next_state <= s00;

  end case;
end process;

Proceso_seleccion: process(present_state,smotor1)
  begin
  case present_state is
  
    when s00 =>
	   smotor1 <= "0000";
	   
    when s0 =>
	   smotor1 <= "1000";

    when s1 =>
	   smotor1 <= "1100";

    when s2 =>
	   smotor1 <= "0100";

    when s3 =>
	   smotor1 <= "0110";

    when s4 =>
	   smotor1 <= "0010";

    when s5 =>
	   smotor1 <= "0011";

    when s6 =>
	   smotor1 <= "0001";

    when s7 =>
	   smotor1 <= "1001";

    when others	=>    smotor1 <= "0000";
    					
  end case;
end process Proceso_seleccion;

Proceso_envio: Process(smotor1)begin

 smotor <= smotor1;
end process Proceso_envio;

end Behavioral;

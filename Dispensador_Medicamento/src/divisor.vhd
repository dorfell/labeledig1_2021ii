library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity divisor is
	port ( clk, reset :  in std_logic;
              en1,enpulso,enpulso2  : out std_logic);
              
end divisor;

architecture Behavioral of divisor is

signal ee, qq : std_logic_vector(27 downto 0):=(others=>'0');
--signal dd : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf       : std_logic;



begin

mux_add: 
  ee <= qq+1  when ovf='0' else
        (others=>'0'); 

-- 1 [Hz]
ovf_com:				    
  ovf <= '1' when qq = x"1E848" else -- 10 [M] 100ns
         '0';
			
			
				  
--rst_mux: 
 --dd <= ee  when rst='0' else -- SIE push
  --      (others=>'0'); 



en_comp :             
  en1 <= '1' when qq < x"F424" else -- 50% 
         '0';
  enpulso <= '1' when qq < x"1" else -- pulso% 
         '0';
         
 enpulso2 <= '1' when qq > x"7A12" and qq < x"7A14" else -- pulso% 
         '0';
process(clk,reset)
begin
if  ( reset='1') then
        qq <= (others=>'0'); 
  else if clk'event and clk='1' then
            qq <= ee;
       else
            qq <= qq;         
          end if;
 end if;
end process;

end Behavioral;

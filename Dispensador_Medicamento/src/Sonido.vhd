library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Sonido is
Port (clk, reset,enpulso_sonido,moto :  in std_logic;
 segmento: in STD_LOGIC_VECTOR (3 downto 0);
             sonidoout  : out std_logic);
end Sonido;

architecture Behavioral of Sonido is
signal enpulso_sonido1: std_logic:= '0';

begin
enpulso_sonido1<=enpulso_sonido;
process( moto,segmento,enpulso_sonido1) begin

	
 if (moto = '1' or segmento="1001")  then
		  sonidoout <= enpulso_sonido1;
		else 
		  sonidoout <= '0';
		end if;	
 end process;

end Behavioral;

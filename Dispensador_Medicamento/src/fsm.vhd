----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2022 19:43:53
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPI is
Port (reset, clk,start,clkfpga : IN STD_LOGIC;
  datos: in STD_LOGIC_VECTOR (15 downto 0);
  cseste : out std_logic;
  cs : out std_logic;
  datoout : out std_logic);
end SPI;

architecture Behavioral of SPI is
type states is (s00,s0,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18);
signal presents, nexts: states;

signal csout: std_logic:= '1';
signal dout: std_logic:= '0';
signal cs2este: std_logic:= '1';

begin

Proceso_estado_presente:Process(reset, clk,start,presents,clkfpga)
Begin
If (reset = '1') then
  presents <= s00;
else if clkfpga'event and clkfpga='1' then
 if ( clk='1') then
  presents <= nexts;
        else
            presents <= presents; 
            end if;
end if;
end if;
end process Proceso_estado_presente;



Proceso_estado_futuro: Process(start,datos,presents)begin
case presents is
  
when s00 =>
  
	   if (start = '1') then
	     csout <= '1';
		   nexts <= s0;
	      cs2este<= '0';
	      dout <= '0';
		else 
		csout <= '1';
		   nexts <= s00;
		    dout <= '0';
		     cs2este<= '0';
		end if;
  
  
  when s0 => nexts <= s1;
	  csout <= '0';
	  dout <= datos(15);
	cs2este<= '0';
  
 when s1 =>
	  nexts <= s2;
	  csout <= '0';
	 dout <= datos(14);
	cs2este<= '0';
	
 when s2 =>
	  nexts <= s3;
csout <= '0';
	 dout <= datos(13);
	cs2este<= '0';
	
 When s3 =>
	  nexts <= s4;
	  csout <= '0';
 dout <= datos(12);
	cs2este<= '0';
	
 When s4 =>
	  nexts <= s5;
	  csout <= '0';
	  dout <= datos(11);
	  cs2este<= '0';
	
 When s5 =>
	  nexts <= s6;
	  csout <= '0';
	  dout <= datos(10);
	  cs2este<= '0';

 When s6 =>
	  nexts <= s7;
	  csout <= '0';
	 dout <= datos(9);
	 cs2este<= '0';
	
When s7 =>
	  nexts <= s8;
	  csout <= '0';
	  dout <= datos(8);
	  cs2este<= '0';
	
 When s8 =>
	  nexts <= s9;
	  csout <= '0';
	 dout <= datos(7);
	 cs2este<= '0';

 When s9 =>
	  nexts <= s10;
	  csout <= '0';
	  dout <= datos(6);
	  cs2este<= '0';

 When s10 =>
	  nexts <= s11;
	  csout <= '0';
	  dout <= datos(5);
	  cs2este<= '0';

 When s11 =>
	  nexts <= s12;
	  csout <= '0';
	  dout <= datos(4);
	  cs2este<= '0';
	
 When s12 =>
	  nexts <= s13;
	  csout <= '0';
	  dout <= datos(3);
	  cs2este<= '0';
		
 When s13 =>
	  nexts <= s14;
	  csout <= '0';
dout <= datos(2);
cs2este<= '0';

 When s14 =>
	  nexts <= s15;
	  csout <= '0';
	 dout <= datos(1);
	 cs2este<= '0';
	
  When s15 =>
	  nexts <= s16;
	  csout <= '0';
	 dout <= datos(0);
	 cs2este<= '0';
	  
 When s16 =>
  nexts <= s17;
	  csout <= '1';
 dout <= '0';
 cs2este<= '0';
 
  When s17 =>
	  nexts <= s18;
	  csout <= '1';
	  dout <='0';
	  cs2este<= '0';
	  
  When s18 =>
	  nexts <= s00;
	  csout <= '1';
 dout <= '0';
 cs2este<= '1';
  end case;

end process Proceso_estado_futuro;

Proceso_envio: Process(csout,dout,cs2este)begin

 cs <= csout;
datoout <= dout;
cseste<=cs2este;
end process Proceso_envio;

end Behavioral;

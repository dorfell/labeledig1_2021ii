----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2022 19:42:31
-- Design Name: 
-- Module Name: Matriz_led - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dispensador is
Port (		
--entradas
      clkfpga, reset,start,moto : in std_logic;
      COLtecla: in std_logic_vector (2 downto 0);
 --salidas
	  clockout : out std_logic;
	  dout	  : out std_logic;
	  cs		  : out std_logic;
	  ROWtecla: out std_logic_vector (3 downto 0);
	   smotor: out std_logic_vector(3 downto 0) ;
	    sonidoout: out std_logic
	  ); 
end Dispensador;



architecture Behavioral of Dispensador is

--componentes
component divisor is
port ( clk,reset  :  in std_logic;
              en1,enpulso,enpulso2  : out std_logic);
 end component divisor;
 
component SPI is
 port (clk,reset,start,clkfpga:  in std_logic;
   datos : in STD_LOGIC_VECTOR (15 downto 0);
   cs : OUT std_logic;
  cseste : OUT std_logic;
   datoout  : out std_logic);
 end component SPI;

component MatrizLed is
Port ( 
reset, clk,start,csretro,clkfpga : IN STD_LOGIC;
  datos1: in STD_LOGIC_VECTOR (15 downto 0);
  datos2: in STD_LOGIC_VECTOR (15 downto 0);
  datos3: in STD_LOGIC_VECTOR (15 downto 0);
  datos4: in STD_LOGIC_VECTOR (15 downto 0);
  datos5: in STD_LOGIC_VECTOR (15 downto 0);
  datos6: in STD_LOGIC_VECTOR (15 downto 0);
  datos7: in STD_LOGIC_VECTOR (15 downto 0);
  datos8: in STD_LOGIC_VECTOR (15 downto 0);
  startout : out std_logic;
  retro : out std_logic;
  datosout: out STD_LOGIC_VECTOR (15 downto 0));
end component MatrizLed;

component Dibujo is
Port ( 
segmento: in STD_LOGIC_VECTOR (3 downto 0);
clave: in STD_LOGIC_VECTOR (1 downto 0);
   retro,clk,clkfpga : in std_logic;
 datos1: out STD_LOGIC_VECTOR (15 downto 0);
  datos2: out STD_LOGIC_VECTOR (15 downto 0);
  datos3: out STD_LOGIC_VECTOR (15 downto 0);
  datos4: out STD_LOGIC_VECTOR (15 downto 0);
  datos5: out STD_LOGIC_VECTOR (15 downto 0);
  datos6: out STD_LOGIC_VECTOR (15 downto 0);
  datos7: out STD_LOGIC_VECTOR (15 downto 0);
  datos8: out STD_LOGIC_VECTOR (15 downto 0)
  );
end component Dibujo;


component KEYPAD
 Port (
        CLK : in std_logic; 
        COL: in std_logic_vector (2 downto 0); 
        ROW: out std_logic_vector (3 downto 0); 
        Sout : out std_logic_vector(3 downto 0)
   );
end component KEYPAD;

component clave
 Port ( 
 segmento: in STD_LOGIC_VECTOR (3 downto 0);
 clk,clkfpga,reset : in std_logic;
 clave: out STD_LOGIC_VECTOR (1 downto 0) );
end component clave;

component Motor
Port (clk,rst,en,moto: in std_logic;
segmento: in STD_LOGIC_VECTOR (3 downto 0);
            smotor: out std_logic_vector(3 downto 0) );
            
end component Motor;

component Divisor_MotorySonido
port ( clk, reset :  in std_logic;
              enpulso_motor,enpulso_sonido  : out std_logic);
end component Divisor_MotorySonido;

component Sonido is
Port (clk, reset,enpulso_sonido,moto:  in std_logic;
 segmento: in STD_LOGIC_VECTOR (3 downto 0);
             sonidoout  : out std_logic);
 end component Sonido;

--se�alesenpulso2
signal enable: std_logic:= '0';
signal enpulso1,enpulso2: std_logic:= '0';
signal datoout1: std_logic_vector(15 downto 0):=(others=>'0');
signal startout,cseste1,retro1: std_logic:= '0';
signal datox1: std_logic_vector(15 downto 0):=(others=>'0');
signal datox2: std_logic_vector(15 downto 0):=(others=>'0');
signal datox3: std_logic_vector(15 downto 0):=(others=>'0');
signal datox4: std_logic_vector(15 downto 0):=(others=>'0');
signal datox5: std_logic_vector(15 downto 0):=(others=>'0');
signal datox6: std_logic_vector(15 downto 0):=(others=>'0');
signal datox7: std_logic_vector(15 downto 0):=(others=>'0');
signal datox8: std_logic_vector(15 downto 0):=(others=>'0');
signal segmteclado: std_logic_vector(3 downto 0):=(others=>'0'); 
signal claveusua: std_logic_vector(1 downto 0):=(others=>'0'); 
signal enpulso_motor,enpulso_sonido: std_logic:= '0'; 
 
begin
--llamado
divisor16: divisor
port map (
 clk  => clkfpga,
reset  => reset,
   en1 => enable,
    enpulso => enpulso1,
       enpulso2 => enpulso2
    );
    
    
 SPI1: SPI
port map (
 clkfpga  => clkfpga,
 clk  => enpulso2,
 reset => reset,
  start =>startout,
   datos  =>datoout1,
    cs => cs,
   cseste=> cseste1,
   datoout  => dout
   );
    
 Maquin: MatrizLed
port map (
reset=> reset,
 clkfpga  => clkfpga,
 clk  => enpulso1,
 start=>start,
 csretro => cseste1,
  datos1=> datox1,
  datos2=> datox2,
  datos3=> datox3,
  datos4=> datox4,
  datos5=> datox5,
  datos6=> datox6,
  datos7=> datox7,
  datos8=> datox8,
  startout =>startout,
   retro =>retro1,
  datosout  =>datoout1
);   

Dibujos: Dibujo
port map (
clkfpga  => clkfpga,
 clk  => enpulso1,
retro=> retro1,
segmento=> segmteclado,
 clave =>claveusua,
 
  datos1=> datox1,
  datos2=> datox2,
  datos3=> datox3,
  datos4=> datox4,
  datos5=> datox5,
  datos6=> datox6,
  datos7=> datox7,
  datos8=> datox8

);   

 Teclado: KEYPAD
port map (
 CLK  => clkfpga,
 COL  => COLtecla,
 ROW => ROWtecla,
  Sout =>segmteclado
   );

 claveusuario: clave
port map (
 segmento  => segmteclado,
clkfpga  => clkfpga,
 clk  => enpulso1,
 reset=> reset,
  clave =>claveusua
   );
    divisor_Motoysoni: Divisor_MotorySonido
port map (
 clk  => clkfpga,
reset  => reset,
   enpulso_motor => enpulso_motor,
    enpulso_sonido => enpulso_sonido
    );

 Motor1: Motor
port map (
clk  => clkfpga,
 rst  => reset,
 en => enpulso_motor,
 segmento=> segmteclado,
  moto => moto,
  smotor =>smotor
  );
      
    Sonido_parlante: Sonido
port map (
 clk  => clkfpga,
reset  => reset,
segmento=> segmteclado,
  moto => moto,
    enpulso_sonido => enpulso_sonido,
    sonidoout=> sonidoout
    );  
    --salidas
    clockout <= enable;
end Behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2022 22:20:57
-- Design Name: 
-- Module Name: Maquina8 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MatrizLed is
Port ( 
reset, clk,start,csretro,clkfpga : IN STD_LOGIC;
  datos1: in STD_LOGIC_VECTOR (15 downto 0);
  datos2: in STD_LOGIC_VECTOR (15 downto 0);
  datos3: in STD_LOGIC_VECTOR (15 downto 0);
  datos4: in STD_LOGIC_VECTOR (15 downto 0);
  datos5: in STD_LOGIC_VECTOR (15 downto 0);
  datos6: in STD_LOGIC_VECTOR (15 downto 0);
  datos7: in STD_LOGIC_VECTOR (15 downto 0);
  datos8: in STD_LOGIC_VECTOR (15 downto 0);
  startout : out std_logic;
   retro: out std_logic;
  datosout: out STD_LOGIC_VECTOR (15 downto 0));
 
end MatrizLed;

architecture Behavioral of MatrizLed is

type states is (s0,s1,s2,s3,s4,s5,s6,s7,s8);
signal presents, nexts: states;
signal datosout1: std_logic_vector(15 downto 0):=(others=>'0');
signal startout1,retro1: std_logic:= '0';


begin

Proceso_cambio_estado: Process(reset, clk,csretro,presents,clkfpga)
Begin
If (reset = '1') then
  presents <= s0;
else if clkfpga'event and clkfpga='1' then
 if ( clk='1' and csretro='1' ) then
  presents <= nexts;
       else
    
            presents <= presents;         
          end if;
end if;
end if;

end process Proceso_cambio_estado;


Proceso_estado_futuro: Process(start,presents,datos1,datos2,datos3,datos4,datos5,datos6,datos7,datos8)begin
case presents is
  
when s0 => 
  if (start = '1') then
	    
		 nexts <= s1;
	      startout1<= '1';----###cambiar a 1
	       datosout1<=datos1;
	      retro1<= '0';
		else 
		   nexts <= s0;
		   startout1 <= '0';
		   datosout1<="0000000000000000";
		    retro1<= '0';
		end if;
	   
	  
	   
 when s1 => 
	    nexts <= s2;
	    datosout1<=datos2;
	   startout1<= '1';
	    retro1<= '0';
 when s2 => 
	  nexts <= s3;
	    datosout1<=datos3;
	   	   startout1<= '1';
	   	   retro1<= '0';
 when s3 => 
	 	 nexts <= s4;
	    datosout1<=datos4;
	   startout1<= '1';
	    retro1<= '0';
 when s4 => 
	 	 nexts <= s5;
	    datosout1<=datos5;
	   startout1<= '1';
	    retro1<= '0';
 when s5 => 
	  nexts <= s6;
	    datosout1<=datos6;
	   startout1<= '1';
	    retro1<= '0';
 when s6 => 
	 nexts <= s7;
	    datosout1<=datos7;
	   startout1<= '1';
	   retro1<= '0';
 when s7 => 
	  nexts <= s8;
	   
	    datosout1<=datos8;
  startout1<= '1';
  retro1<= '0';
  
    when s8 => 
	  nexts <= s0;
	 --   datosout1<="0000101000001111"; --Set intensity to maximum;
	    datosout1<="0000000000000000"; --Set intensity to maximum;
  startout1<= '1';
  retro1<= '1';
   
  end case;


end process Proceso_estado_futuro;


Proceso_envio: Process(datosout1,startout1,retro1)begin

 startout <= startout1;
datosout <= datosout1;
retro<= retro1;
end process Proceso_envio;


end Behavioral;

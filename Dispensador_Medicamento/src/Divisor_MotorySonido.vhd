library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;



entity Divisor_MotorySonido is
	port ( clk, reset :  in std_logic;
              enpulso_motor,enpulso_sonido  : out std_logic);
end Divisor_MotorySonido;

architecture Behavioral of Divisor_MotorySonido is
signal ee, qq, ee1, qq1 : std_logic_vector(27 downto 0):=(others=>'0');
--signal dd : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf,ovfmoto: std_logic;



begin

mux_add: 
  ee <= qq+1  when ovf='0' else
        (others=>'0'); 
 mux_add2: 
  ee1 <= qq1+1  when ovfmoto='0' else
        (others=>'0'); 

-- 1 [Hz]
ovf_com:				    
  ovf <= '1' when qq = x"455BA" else -- 10 [M] 100ns
         '0';
         
ovf_motor:	
ovfmoto <= '1' when qq1 = x"1DCD650" else -- 10 [M] 100ns
         '0';	
			
				  
--rst_mux: 
 --dd <= ee  when rst='0' else -- SIE push
  --      (others=>'0'); 



en_comp :             
 
  enpulso_sonido <= '1' when qq < x"22ADD" else -- 50%
         '0';
   enpulso_motor <= '0' when qq1 < x"1DCD650" else -- pulso%  
         '1';  
divisor1:process(clk,reset)
begin
if  ( reset='1') then
        qq <= (others=>'0'); 
  else if clk'event and clk='1' then
            qq <= ee;
       else
            qq <= qq;         
          end if;
 end if;
end process divisor1;

divisor2:process(clk,reset)
begin
if  ( reset='1') then
        qq1 <= (others=>'0'); 
  else if clk'event and clk='1' then
            qq1 <= ee1;
       else
            qq1 <= qq1;         
          end if;
 end if;
end process divisor2;

end Behavioral;
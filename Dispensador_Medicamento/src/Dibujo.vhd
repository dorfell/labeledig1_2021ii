----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2022 20:44:07
-- Design Name: 
-- Module Name: Dibujo - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Dibujo is
 Port (  
 segmento: in STD_LOGIC_VECTOR (3 downto 0);
 clave: in STD_LOGIC_VECTOR (1 downto 0);
   retro,clk,clkfpga : in std_logic;
 datos1: out STD_LOGIC_VECTOR (15 downto 0);
  datos2: out STD_LOGIC_VECTOR (15 downto 0);
  datos3: out STD_LOGIC_VECTOR (15 downto 0);
  datos4: out STD_LOGIC_VECTOR (15 downto 0);
  datos5: out STD_LOGIC_VECTOR (15 downto 0);
  datos6: out STD_LOGIC_VECTOR (15 downto 0);
  datos7: out STD_LOGIC_VECTOR (15 downto 0);
  datos8: out STD_LOGIC_VECTOR (15 downto 0)
   );
end Dibujo;



architecture Behavioral of Dibujo is
signal matrizready: std_logic_vector(1 downto 0):=(others=>'0');
signal datox1: std_logic_vector(15 downto 0):=(others=>'0');
signal datox2: std_logic_vector(15 downto 0):=(others=>'0');
signal datox3: std_logic_vector(15 downto 0):=(others=>'0');
signal datox4: std_logic_vector(15 downto 0):=(others=>'0');
signal datox5: std_logic_vector(15 downto 0):=(others=>'0');
signal datox6: std_logic_vector(15 downto 0):=(others=>'0');
signal datox7: std_logic_vector(15 downto 0):=(others=>'0');
signal datox8: std_logic_vector(15 downto 0):=(others=>'0');

begin

Proceso_dibujo:Process(clave, clk,clkfpga,retro,segmento,matrizready,datox1,datox2,datox3,datox4,datox5,datox6,datox7,datox8) begin

--inicializacion 
    if clkfpga'event and clkfpga='1' then
 if ( clk='1') then
 
if matrizready ="00" then
	   
		 datox1 <= "0000110000000000";--Shut down
	     datox2<="0000101100000111"; --scan limit 
	     datox3<="0000101000001111"; --Set intensity to maximum
	     datox4<= "0000100100000000"; --decode mode
	     datox5<= "0000110000000001"; --normal
	     datox6<= "0000000000000001"; --clear row 1
	     datox7<= "0000000000000001"; --clear row 2
	     datox8<= "0000000000000001"; --clear row 3
	     matrizready <="01";
	     
		elsif  matrizready="01" and retro='1' then
			 datox1 <= "0000010000000000"; --clear row 4
	     datox2<= "0000010101010101"; --clear row 5
	     datox3<= "0000011001010101"; --clear row 6
	     datox4<= "0000011101010101"; --clear row 7
	     datox5<= "0000100001010101"; --clear row 8
	     datox6<= "0000100000000000"; --clear row 8
	     datox7<= "0000100001010101"; --clear row 8
	     datox8<= "0000100000000000"; --clear row 8
	      matrizready <="11";
 	      elsif matrizready ="11" and retro='1' and clave ="01" then
case segmento is
					when "0000" => --
		datox1 <= "00000001"&"10101010"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"10101010"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"10101010"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"10101010"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "0001" => 
    datox1 <="00000001"&"00001000"; --clear row 1
    datox2 <="00000010"&"00011000"; --clear row 2
    datox3 <="00000011"&"00101000"; --clear row 3
    datox4 <="00000100"&"00001000"; --clear row 4
    datox5 <="00000101"&"00001000"; --clear row 5
    datox6 <="00000110"&"00001000"; --clear row 6
    datox7 <="00000111"&"00001000"; --clear row 7
    datox8 <="00001000"&"00111110"; --clear row 8
	          matrizready <="11";
	     
					when "0010" => 
		datox1 <="00000001"&"00011000"; --clear row 1
        datox2 <="00000010"&"00100100"; --clear row 2
        datox3 <="00000011"&"01000010"; --clear row 3
        datox4 <="00000100"&"00000100"; --clear row 4
        datox5 <="00000101"&"00001000"; --clear row 5
        datox6 <="00000110"&"00010000"; --clear row 6
        datox7 <="00000111"&"00100000"; --clear row 7
        datox8 <="00001000"&"11111111"; --clear row 8
	          matrizready <="11";
	     
					when "0011" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"11111111"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"11111111"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"11111111"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"11111111"; --clear row 8
	          matrizready <="11";
	     
					when "0100" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
					when "0101" =>
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "0110" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "0111" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "1000" =>
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "1001" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "1011" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "1100" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
	     
					when "1101" => 
		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2<="00000010"&"01010101"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"01010101"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"01010101"; --clear row 8
	          matrizready <="11";
				when others => 
	datox1 <= datox1;
	     datox2<=datox2; 
	     datox3<=datox3; 
	     datox4<= datox4; 
	     datox5<= datox5; 
	     datox6<= datox6; 
	     datox7<= datox7; 
	     datox8<= datox8;
	      matrizready <=matrizready;
				end case;
elsif matrizready ="11" and retro='1' and clave ="00" then

		 datox1 <= "00000001"&"11111111"; --clear row 1
	     datox2 <="00000010"&"11111111"; --clear row 2
	     datox3<="00000011"&"11111111"; --clear row 3
	     datox4<= "00000100"&"01010101"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"11111111"; --clear row 6
	     datox7<= "00000111"&"11111111"; --clear row 7
	     datox8<= "00001000"&"11111111"; --clear row 8
	          matrizready <="11";
elsif matrizready ="11" and retro='1' and clave ="11" then

		 datox1 <= "00000001"&"11000011"; --clear row 1
	     datox2 <="00000010"&"11000011"; --clear row 2
	     datox3<="00000011"&"11000011"; --clear row 3
	     datox4<= "00000100"&"11111111"; --clear row 4
	     datox5<= "00000101"&"11111111"; --clear row 5
	     datox6<= "00000110"&"11000011"; --clear row 6
	     datox7<= "00000111"&"11000011"; --clear row 7
	     datox8<= "00001000"&"11000011"; --clear row 8
	          matrizready <="11";
	      else
	datox1 <= datox1;
	     datox2<=datox2; 
	     datox3<=datox3; 
	     datox4<= datox4; 
	     datox5<= datox5; 
	     datox6<= datox6; 
	     datox7<= datox7; 
	     datox8<= datox8;
	      matrizready <=matrizready;
	      end if;
	            else
	datox1 <= datox1;
	     datox2<=datox2; 
	     datox3<=datox3; 
	     datox4<= datox4; 
	     datox5<= datox5; 
	     datox6<= datox6; 
	     datox7<= datox7; 
	     datox8<= datox8;
	      matrizready <=matrizready;
	      end if;
	       end if;
--datos a dibujar segun teclado matricial 

end process Proceso_dibujo;

Proceso_envio: Process(datox1,datox2,datox3,datox4,datox5,datox6,datox7,datox8)begin

 datos1 <= datox1;
datos2 <= datox2;
datos3 <= datox3;
datos4 <= datox4;
datos5 <= datox5;
datos6 <= datox6;
datos7 <= datox7;
datos8 <= datox8;

end process Proceso_envio;


end Behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.01.2022 23:17:22
-- Design Name: 
-- Module Name: clave - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clave is
 Port ( 
 segmento: in STD_LOGIC_VECTOR (3 downto 0);
 clk,clkfpga,reset : in std_logic;
 clave: out STD_LOGIC_VECTOR (1 downto 0) );
end clave;

architecture Behavioral of clave is

type states is (s0,s1,s2,s3,s4,serror);
signal presents, nexts: states;
signal clave1: std_logic_vector(1 downto 0):=(others=>'0');


begin

Proceso_cambio_estado: Process(reset,clk,presents,clkfpga)
Begin
If (reset = '1') then
  presents <= s0;
else if clkfpga'event and clkfpga='1' then
 if  clk='1' then
  presents <= nexts;
       else
    
            presents <= presents;         
          end if;
end if;
end if;

end process Proceso_cambio_estado;


Proceso_estado_futuro: Process(segmento,presents)begin
case presents is
  
when s0 => 
  if (segmento = "1010") then
	    nexts <= s1;
    clave1<="01";
		else 
		   nexts <= s0;
		   clave1<="00";
		end if;
	   
	  
	   
 when s1 => --clave 1
	  If (segmento = "1010") then
  nexts <= s1;
   clave1<="01";
else if (segmento = "0001") then
  nexts <= s2;
   clave1<="01";
       else
            nexts <= serror; 
             clave1<="00";       
          end if;
end if;

 when s2 => --clave 2
	  	  If (segmento = "0001") then
  nexts <= s2;
   clave1<="01";
else if (segmento = "0010") then
  nexts <= s3;
   clave1<="01";
       else
            nexts <= serror;    
             clave1<="00";    
          end if;
end if;

 when s3 => --clave 3
	 		  If (segmento = "0010") then
  nexts <= s3;
   clave1<="01";
else if (segmento = "0011") then
  nexts <= s4;
   clave1<="01";
       else
            nexts <= serror;  
             clave1<="00";      
          end if;
end if;

 when s4 => --clave 4
	 		  If (segmento = "0011") then
  nexts <= s4;
   clave1<="01";
else if (segmento = "0100") then
  nexts <= s0;
   clave1<="01";
       else
            nexts <= serror; 
             clave1<="00";       
          end if;
end if;

 when serror => 
	  nexts <= s0;
	  clave1<="00";
	  
   
  end case;


end process Proceso_estado_futuro;


Proceso_envio: Process(clave1)begin

 clave <= clave1;

end process Proceso_envio;


end Behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.01.2022 20:09:12
-- Design Name: 
-- Module Name: simulacion_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity simulacion_tb is
-- Port ( );
end simulacion_tb;

architecture Behavioral of simulacion_tb is

COMPONENT Dispensador
    PORT(
        clkfpga, reset, start,moto :  in std_logic;
         COLtecla: in std_logic_vector (2 downto 0);
         
         clockout, dout,  cs : out std_logic;
          ROWtecla: out std_logic_vector (3 downto 0);
            smotor: out std_logic_vector(3 downto 0) ;
	    sonidoout: out std_logic
    );
    END COMPONENT;
 -- Entradas
    signal clkfpga,reset,start,moto  : std_logic := '0';
    signal COLtecla: std_logic_vector(2 downto 0):=(others=>'0');
    -- Salidas
    signal clockout, dout,  cs   : std_logic := '0';
    signal ROWtecla: std_logic_vector(3 downto 0):=(others=>'0'); 
    signal smotor: std_logic_vector(3 downto 0):=(others=>'0'); 
    signal sonidoout : std_logic := '0';
       --signal datot  : std_logic_vector (3 downto 0) :="0000";
    constant entrada_t : time := 8 ns;
    
begin
 -- Instancia de la unidad bajo prueba.
    uut: Dispensador PORT MAP (
        clkfpga => clkfpga,
        moto=>moto,
        reset => reset,
        start => start,
         COLtecla => COLtecla,
          ROWtecla => ROWtecla,
        clockout => clockout,
        smotor =>smotor,
        dout => dout,
        sonidoout => sonidoout,
        cs => cs
    );
 -- Definición del reloj.
    entrada_process :process
        begin
        clkfpga <= '0';
        wait for entrada_t / 2;
        clkfpga <= '1';
        wait for entrada_t / 2;
    end process;
 
    -- Procesamiento de estímulos.
    estimulos: process
    begin
         wait  for 10 ns;
       
        
	-- rst <= '1'; -- Condiciones iniciales.
      --  wait for 100 ns;
     
	--valores de simulacion de APPEND_MODE
   -- rst <= '0';
    start <= '1';
    wait for 80 ms;
   --   start <= '0';
   COLtecla <= "001";
    moto <= '1';
         wait for 150 ms;
       start <= '0';
      
        COLtecla <= "000";
        
        wait for 150 ms;
         COLtecla <= "010";
       start <= '1';
    wait;  
    end process;
end Behavioral;

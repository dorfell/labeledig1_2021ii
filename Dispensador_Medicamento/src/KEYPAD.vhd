library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KEYPAD is
  Port (
        CLK : in std_logic; 
        COL: in std_logic_vector (2 downto 0); 
        ROW: out std_logic_vector (3 downto 0); 
        Sout : out std_logic_vector(3 downto 0)
   );
end KEYPAD;

architecture Behavioral of KEYPAD is

component LOGIC_KEYPAD is
GENERIC(
			FREQ_CLK : INTEGER := 125_000_000         --FRECUENCIA DE LA TARJETA
);

PORT(
	CLK 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	BOTON_PRES : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO QUE INDICA LA TECLA QUE SE PRESION�
	IND		  : OUT STD_LOGIC					   --BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ)
);
end component LOGIC_KEYPAD; 

signal BOTON_PRES: std_logic_vector(3 downto 0) := (others => '0');
signal IND: std_logic := '0'; 
signal segm: std_logic_vector (3 downto 0) := (others => '0');  

begin

inst_lib_teclado: LOGIC_KEYPAD

    generic map(
        FREQ_CLK => 125000000
    )
    port map(
        CLK => CLK, 
        COLUMNAS => COL, 
        FILAS => ROW,
        BOTON_PRES => BOTON_PRES,
        IND => IND
    );
    
process_KEYPAD: process (CLK, IND, BOTON_PRES, segm)
 begin  
    if rising_edge(CLK) then
       if IND='1' and BOTON_PRES= x"0" then segm<="0000";
        elsif IND='1' and BOTON_PRES= x"1" then segm<="0001";
        elsif IND='1' and BOTON_PRES= x"2" then segm<="0010";
        elsif IND='1' and BOTON_PRES= x"3" then segm<="0011";
        elsif IND='1' and BOTON_PRES= x"4" then segm<="0100";
        elsif IND='1' and BOTON_PRES= x"5" then segm<="0101";
        elsif IND='1' and BOTON_PRES= x"6" then segm<="0110";
        elsif IND='1' and BOTON_PRES= x"7" then segm<="0111";
        elsif IND='1' and BOTON_PRES= x"8" then segm<="1000";
        elsif IND='1' and BOTON_PRES= x"9" then segm<="1001";
        elsif IND='1' and BOTON_PRES= x"E" then segm<="1010";
        elsif IND='1' and BOTON_PRES= x"F" then segm<="1011";
        else segm <= segm; 
     end if; 
   end if;  
end process; 


Sout <= segm; 

end Behavioral;


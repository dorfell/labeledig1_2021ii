----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2022 09:16:45 PM
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm is
 generic(attempts: integer := 3;
         UART_KEY: std_logic_vector(7 downto 0) := x"30");
  Port (clk: in std_logic;
        rst: in std_logic;
        in_char: in std_logic_vector(7 downto 0);
        flag_char: in std_logic;
        line1: out std_logic_vector(127 downto 0);
        line2: out std_logic_vector(127 downto 0);
        start_bt: in std_logic;
        password: in std_logic_vector(31 downto 0);
        buzzer: out std_logic;
        rd_uart, wr_uart: out std_logic;
        empty_uart: in std_logic;
        get_data_uart: in std_logic_vector(7 downto 0);
        send_data_uart: out std_logic_vector(7 downto 0);
        en_motor: out std_logic);
end fsm;

architecture Behavioral of fsm is
signal right_pass,wrong_pass: std_logic;
signal right_pass_next,wrong_pass_next: std_logic;
signal line1_buff,line2_buff: std_logic_vector(127 downto 0);

signal en, en_next: std_logic := '0';

signal count_attempts: integer range 0 to 5 := 0;
signal count_attempts_next: integer range 0 to 5 := 0;


type states_k is (start,typing,open_d,w_msg,alert_protocol);
signal state_reg,state_next: states_k;

signal right_pass_uart: std_logic := '0';
signal send_uart: std_logic := '0'; 

signal en_motor_n: std_logic := '0';
signal wait_motor: integer := 125000000*3;
signal flag_motor: std_logic := '0';

begin

-- pass -key verification
pass_man: entity work.pass(Behavioral)
port map(clk => clk, rst => rst,password => password,in_data =>in_char,
flagIn => flag_char,right_pass => right_pass_next,wrong_pass => wrong_pass_next,en => en_next); 

uart_coupling: entity work.uart_key(Behavioral)
generic map( UART_KEY => UART_KEY)
port map(clk => clk, empty_uart => empty_uart, clear => rd_uart, get_data_uart => get_data_uart,
         right_key => right_pass_uart);

tick_gen1: entity work.debounce_circuit(Behavioral)
port map(clk => clk, reset => rst, sw => send_uart, db_tick => wr_uart);

--reg
process(clk)
begin
if rst = '1' then
    state_reg <= start;
elsif rising_edge(clk) then
    state_reg <= state_next;
    line1 <= line1_buff;
    line2 <= line2_buff;
    right_pass <= right_pass_next;
    wrong_pass <= wrong_pass_next;
    en <= en_next;
    en_motor <= en_motor_n;
end if;
end process;












--next state logic

process(state_reg,start_bt,flag_char,right_pass,wrong_pass,right_pass_uart,flag_motor)

begin
state_next <= state_reg;
--right_pass_next <= right_pass;
--wrong_pass_next <= wrong_pass;
--en_next <= en;

case state_reg is

when start =>
    count_attempts <= 0;
    if start_bt = '1' then
        state_next <= typing;
    else
        state_next <= start;    
    end if;
when typing =>
        
    if right_pass = '1'  or right_pass_uart = '1' then
        state_next <= open_d;
        count_attempts <= 0;
    elsif wrong_pass = '1' then
        count_attempts <= count_attempts +1;
        if count_attempts <= attempts then
            state_next <= w_msg;
            --state_next <= typing;
        else
            state_next <= alert_protocol;
        end if;
     else            
    end if;
    
when w_msg =>
    if flag_char = '1' then
        state_next <= typing;
    end if;
when open_d =>
       if flag_motor = '1' then
          state_next <= typing;
       end if;
when alert_protocol =>
     if right_pass_uart = '1' then
        state_next <= open_d;
     end if;  
end case;

end process;


---output logic

process(state_reg,right_pass,wrong_pass,flag_char, flag_motor)
variable count_LCD_char: integer range 0 to 4:= 0;
begin

case state_reg is
    when start =>
        send_uart <= '0';
        buzzer <= '0';
        line1_buff <= x"4269656E76656E696440202020202020";
        line2_buff <= x"70726573696F6E652027737461727427";
        en_next <= '0';
        
    when typing =>
        en_motor_n <= '0';
        send_uart <= '0';
    
        buzzer <= '0';
    
        line1_buff <= x"496E6772657365206C6120636C617665";
        en_next <= '1';
        if count_LCD_char = 0 then
            line2_buff <= x"3E202020202020202020202020202020";
        end if;
        
        if flag_char ='1' and  right_pass /= '1' and  wrong_pass /= '1' then
        --if flag_char ='1' then  
          
        count_LCD_char := count_LCD_char +1;
            if count_LCD_char = 1 then
                line2_buff(119 downto 112) <= in_char;
            elsif count_LCD_char = 2 then
                line2_buff(111 downto 104) <= in_char;
            elsif count_LCD_char = 3 then
                line2_buff(103 downto 96) <= in_char;
            elsif count_LCD_char = 4 then
                line2_buff(95 downto 88) <= in_char;
            end if;
        end if;
        
     when w_msg =>
        count_LCD_char := 0;
        line1_buff <= x"636C61766520696E636F727265637461";
        line2_buff <= x"3E202020202020202020202020202020";
            
    when open_d =>
        send_uart <= '0';
        count_LCD_char := 0;
        en_next <= '0';
        line1_buff <= x"436C61766520636F7272656374612020";
        line2_buff <= x"61627269656E646F2E2E2E2020202020"; 
        
        en_motor_n <= '1';
        
        if flag_motor = '1' then
            en_motor_n <= '0';
        end if;
                     
        
    when alert_protocol =>
        en_next <= '0'; 
        buzzer <= '1';
        send_data_uart <= x"30";
        send_uart <= '1'; 
        line1_buff <= x"416C6572746121212020202020202020";
        line2_buff <= x"446573626C6F7175656F20424C544820";
end case;
end process;


--motor counter
process(clk, en_motor_n)

variable count_m: integer := 0;
begin
    if rising_edge(clk) then
      if en_motor_n = '1' then
        if count_m = wait_motor then
            flag_motor <= '1';
            count_m := 0;
        else
            count_m := count_m +1;
            flag_motor <= '0';
        end if;
      else
         flag_motor <= '0'; 
       end if;           
    end if;
end process;


end Behavioral;

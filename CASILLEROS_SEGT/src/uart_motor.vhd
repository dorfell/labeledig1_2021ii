library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity uart_motor is
  Port ( 
    clk, rst, start, dir: in std_logic;
    rd_uart, wr_uart: in std_logic; -- Para el FIFO
	rx: in std_logic;
	w_data: in std_logic_vector(7 downto 0);
	tx_full, rx_empty: out std_logic;
	tx: out std_logic;
    S: out std_logic_vector(3 downto 0)
    );
end uart_motor;

architecture Behavioral of uart_motor is

signal stop: std_logic_vector(7 downto 0);

begin

Inst_UART : entity work.UART
port map (
	clk => clk,
	rst => rst,
	rd_uart => rd_uart,
	wr_uart => wr_uart,
	rx => rx,
	w_data => w_data,
	tx_full => tx_full,
	rx_empty => rx_empty,
	tx => tx,
	r_data => stop
);

Inst_Motor: entity work.Motor
port map ( 
    clk => clk,
    rst => rst,
    stop => stop,
    start => start,
    dir => dir,
    S => S
);

end behavioral;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSM1 is
     Port (clk,rst,en,dir,start: in std_logic;
            stop: in std_logic_vector(7 downto 0); 
            S: out std_logic_vector(3 downto 0));
end FSM1;

architecture Behavioral of FSM1 is
    type states is (inicio,s0,s1,s2,s3,s4,s5,s6,s7);
    signal present_state, next_state: states;

--Tener cuidado con la llegada del STOP

begin
process(clk, rst)
  begin
      if rst = '1' then
        present_state <= inicio;
      elsif (clk'event and clk='1') then
        if en='1' then
           present_state <= next_state;
        else		
           present_state <= present_state;
         end if;
      end if;
 end process;


process(present_state, start, dir, stop)
  begin 
  case present_state is 
  
    when inicio =>
      if stop="01110011" then               
       if (start = '1') then
           if dir='1' then
               next_state <= s0;
            else 
               next_state <= s0;
            end if;
       end if;
     end if; 

    when s0 =>
        if stop="01110011"  then
           if dir='1' then
               next_state <= s1;
           else 
               next_state <= s7;
           end if; 
        else 
            next_state <= inicio;
        end if; 
  

    when s1 =>
        if stop="01110011"  then
            if dir='1' then
               next_state <= s2;
            else 
               next_state <= s0;
           end if;
        else 
            next_state <= inicio;
        end if; 	    

    when s2 =>
        if stop="01110011"  then
            if dir='1' then
               next_state <= s3;
            else 
               next_state <= s1;
           end if;
        else 
            next_state <= inicio;
        end if;
        
    when s3 =>
        if stop="01110011"  then
            if dir='1' then
               next_state <= s4;
            else 
               next_state <= s2;
           end if;
         else 
            next_state <= inicio;
        end if;
                  
	when s4 =>
	   if stop="01110011"  then
            if dir='1' then
               next_state <= s5;
            else 
               next_state <= s3;
           end if;
         else 
            next_state <= inicio;
        end if;
        
	when s5 =>
	   if stop="01110011" then
            if dir='1' then
               next_state <= s6;
            else 
               next_state <= s4;
           end if;
        else 
                next_state <= inicio;
        end if;
	   
	when s6 =>
	   if stop="01110011" then
            if dir='1' then
               next_state <= s7;
            else 
               next_state <= s5;
           end if;
        else 
                next_state <= inicio;
        end if;
        
        	   
	when s7 =>
        if stop="01110011" then
            if dir='1' then
               next_state <= s0;
            else 
               next_state <= s6;
           end if;
        else 
                next_state <= inicio;
        end if;
        	      
  end case;
end process;

process(present_state)
  begin
  case present_state is
  
    when inicio =>
	   S <= "0000";
	   
    when s0 =>
	   S <= "1000";	   

    when s1 =>
	   S <= "1100";

    when s2 =>
	   S <= "0100";

    when s3 =>
	   S <= "0110";

    when s4 =>
	   S <= "0010";

    when s5 =>
	   S <= "0011";

    when s6 =>
	   S <= "0001";
	   
    when s7 =>
	   S <= "1001";

    when others	=> null;
    					
  end case;
end process;

end Behavioral;
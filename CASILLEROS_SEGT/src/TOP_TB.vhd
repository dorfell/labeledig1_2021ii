library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP_TB is
--  Port ( );
end TOP_TB;

architecture Behavioral of TOP_TB is
--DECLARACIÓN DEL COMPOENENTE
COMPONENT TOP
  Port ( 
    clk 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	columns   : IN  STD_LOGIC_VECTOR(2  DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	rst : in std_logic;  
	rows : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	lcd_e  : out std_logic;
    lcd_rs : out std_logic;
    lcd_rw : out std_logic;
    lcd_db : out std_logic_vector(7 downto 4);
    S_motor: out std_logic_vector(3 downto 0));
END COMPONENT;

--ENTRADAS
signal clk: std_logic; 
signal rst: std_logic; 
signal columns: std_logic_vector(2 downto 0); 


--SALIDAS
signal rows: STD_LOGIC_VECTOR(3 DOWNTO 0);
signal lcd_e: std_logic;
signal lcd_rs: std_logic;
signal lcd_rw: std_logic;
signal lcd_db: std_logic_vector(7 downto 4);
signal S_motor: std_logic_vector(3 downto 0);


begin

--INSTANCIA DEL COMPONENTE
PRUEBA: TOP
port map(
        clk =>clk,
        columns => columns,
        rows => rows,
        lcd_e => lcd_e,
        lcd_rs => lcd_rs,
        lcd_rw => lcd_rw,
        lcd_db => lcd_db,
        rst => rst,
        S_motor => S_motor
);

rst <= '0';  


clk_process: process
    begin        
        clk <= '0';
        wait for 4 ns;
        clk <= '1';
        wait for 4 ns;
end process;


 F3_PROC: PROCESS
 BEGIN
    WAIT FOR 30 MS; 
    columns <= "100";
    WAIT; 
 END PROCESS; 

end Behavioral;

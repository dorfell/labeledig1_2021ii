library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity LOGIC_LCD is
  port (
    clk    : in  std_logic;
    lcd_e  : out std_logic;
    lcd_rs : out std_logic;
    lcd_rw : out std_logic;
    lcd_db : out std_logic_vector(7 downto 4);
    dir : out std_logic;
    start : out std_logic;
    en_buzzer : out std_logic;
    data_entry : in std_logic_vector(7 downto 0); --DATOS DE ENTRADA
    data_flag: in std_logic); --INDICA QUE SE PULSA UN BOTON


end entity LOGIC_LCD;

-------------------------------------------------------------------------------

architecture behavior of LOGIC_LCD is

  -- 
  signal timer : natural range 0 to 100000000 := 0;
  signal switch_lines : std_logic := '0';
  signal line1 : std_logic_vector(127 downto 0); --16 POSICIONES, CADA UNA DE 8 BITS
  signal line2 : std_logic_vector(127 downto 0); --16 POSICIONES, CADA UNA DE 8 BITS
  signal count : integer range 0 to 10:= 0;

  -- component generics
  constant CLK_PERIOD_NS : positive := 8;  -- 125 Mhz

  -- component ports
  signal rst          : std_logic;
  signal line1_buffer : std_logic_vector(127 downto 0) := (others => '0');--SE�ALES: TRANSPORTAN EL MENSAJE
  signal line2_buffer : std_logic_vector(127 downto 0) := (others => '0');
  signal clave: std_logic_vector(7 downto 0); --SE ALMACENA LO INGRESADO EN EL TECLADO
  --signal confirmacion: std_logic_vector(7 downto 0); 
  signal pulse: std_logic_vector(7 downto 0); --CONFIRMA EL PULSO *
  signal enter: std_logic_vector(7 downto 0); --CAMBIO DE ESTADOS
  --signal ind_clave: std_logic; 
  signal espacio: std_logic_vector(119 downto 0) := x"202020202020202020202020202020";
  
  --SE�ALES DE MAQUINA DE ESTADOS
    type states is (inicio,s0,s1,s2);
    signal present_state, next_state: states;

begin  -- architecture behavior

  -- component instantiation
  DUT : entity work.CONTROLLER_LCD
    generic map (
      CLK_PERIOD_NS => CLK_PERIOD_NS)
    port map (
      clk          => clk,
      rst          => rst,
      lcd_e        => lcd_e,
      lcd_rs       => lcd_rs,
      lcd_rw       => lcd_rw,
      lcd_db       => lcd_db,
      line1_buffer => line1_buffer,
      line2_buffer => line2_buffer);

 rst <= '0';


--ESCRITURA EN LA LCD
process(clk,data_flag)
  begin
    if  rising_edge(clk)  and data_flag = '1' then
       if count < 1 then
            if count = 0 then
               clave <= data_entry;
               pulse <= data_entry; 
               enter <= data_entry;
            end if;
            count <= count + 1;
            
       else
            count <= 0;
       end if;
    end if;
end process; 

--confirmacion <= clave; 

--COMPARACI�N 
--process(clave)
--begin
--    if clave = x"32" then 
--        ind_clave <= '1'; 
--    else 
--        ind_clave <= '0';     
--    end if; 
--end process; 

--Registro
process(clk)
begin 
    if clk'event and clk='1' then 
        line1_buffer <= line1;  
        line2_buffer <= line2; 
    end if; 
end process; 




--MAQUINA DE ESTADOS
process(clk,data_flag)
  begin
    present_state <= inicio;
  if ((clk'event and clk='1') and data_flag='1') then      
	   present_state <= next_state;
    else		
	   present_state <= present_state;
  end if;
end process;


--PROXIMO ESTADO
process(present_state, clave, pulse, enter)
  begin 
  case present_state is 
    when inicio =>
	   if enter = x"23" then
		   next_state <= s0;
		else 
		   next_state <= s2;
       end if;
  
    when s0 =>
	   if enter = x"23" then
		   next_state <= s1;
	   else 
		   next_state <= s0;
	   end if; 

    when s1 =>
        if pulse = x"2A" then
	       next_state <= inicio;
	    else 
		   next_state <= s1;
	   end if;
	    

    when s2 =>
        if enter = x"23" then
	       next_state <= inicio;
	    else 
		   next_state <= s2;
	   end if;
	   	      
  end case;
end process;

--ACCIONES ESTADO PRESENTE
process(present_state)
  begin
  case present_state is
  
    when inicio =>
	   --MENSAJE PREDETERMINADO: INGRESAR CLAVE
        line1 <= x"494E47524553415220434C4156452020";
        line2(127 downto 120) <= clave;    
        line2(119 downto 0) <= espacio;    
	    
    when s0 =>
	  	--MENSAJE PREDETERMINADO: ABRIENDO LOCKER.
        line1 <= x"41425249454E444F204C4F434B45522E";


	   	--MENSAJE PREDETERMINADO: PULSE #
        line2 <= x"50554C53452023202020202020202020";
        
        start <= '1';
        dir <= '1'; 
        en_buzzer <= '1'; 

    when s1 =>
	  	--MENSAJE PREDETERMINADO: PULSE * PARA 
        line1 <= x"50554C5345202A205041524120202020";
           
        --MENSAJE PREDETERMINADO: CERRAR LOCKER. 
        line2 <= x"434552524152204C4F434B45522E2020";
        	

        start <= '1'; 
        dir <= '0'; 
        en_buzzer <= '0';        

    when s2 =>
	   	--MENSAJE PREDETERMINADO: CLAVE INCORRECTA
        line1 <= x"434C41564520494E434F525245435441";
        
	   	--MENSAJE PREDETERMINADO: PULSE #
        line2 <= x"50554C53452023202020202020202020";

    when others	=> null;
    					
  end case;
end process;

end architecture behavior;

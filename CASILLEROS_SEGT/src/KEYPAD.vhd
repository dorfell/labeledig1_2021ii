library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Keypad is
Port (
        CLK : in std_logic; 
        COL: in std_logic_vector (2 downto 0); 
        ROW: out std_logic_vector (3 downto 0); 
        Sout : out std_logic_vector(3 downto 0);
        FLAG: out std_logic
   );
end KEYPAD;

architecture Behavioral of KEYPAD is

--DECLARACI�N COMPONENTE
component LOGIC_KEYPAD is
GENERIC(
			FREQ_CLK : INTEGER := 125_000_000         --FRECUENCIA DE LA TARJETA
);

PORT(
	CLK 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	COLUMNAS   : IN  STD_LOGIC_VECTOR(2 DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	FILAS 	  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	BOTON_PRES : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --PUERTO QUE INDICA LA TECLA QUE SE PRESION�
	IND		  : OUT STD_LOGIC					   --BANDERA QUE INDICA CUANDO SE PRESION� UNA TECLA (S�LO DURA UN CICLO DE RELOJ)
);
end component LOGIC_KEYPAD; 

--SE�ALES
signal BOTON_PRES: std_logic_vector(3 downto 0) := (others => '0');
signal IND: std_logic := '0'; 
signal segm: std_logic_vector (3 downto 0) := x"0";  

begin

--INSTANCIA COMPONENTE
inst_lib_teclado: LOGIC_KEYPAD

    generic map(
        FREQ_CLK => 125000000
    )
    port map(
        CLK => CLK, 
        COLUMNAS => COL, 
        FILAS => ROW,
        BOTON_PRES => BOTON_PRES,
        IND => IND
    );


--PROCESO TECLADO
process_KEYPAD: process (CLK, IND, BOTON_PRES, segm)
 begin  
    if rising_edge(CLK) then
       if IND='1' and     BOTON_PRES= x"0" then segm<=x"0";
        elsif IND='1' and BOTON_PRES= x"1" then segm<=x"1";
        elsif IND='1' and BOTON_PRES= x"2" then segm<=x"2";
        elsif IND='1' and BOTON_PRES= x"3" then segm<=x"3";
        elsif IND='1' and BOTON_PRES= x"4" then segm<=x"4";
        elsif IND='1' and BOTON_PRES= x"5" then segm<=x"5";
        elsif IND='1' and BOTON_PRES= x"6" then segm<=x"6";
        elsif IND='1' and BOTON_PRES= x"7" then segm<=x"7";
        elsif IND='1' and BOTON_PRES= x"8" then segm<=x"8";
        elsif IND='1' and BOTON_PRES= x"9" then segm<= x"9";
        elsif IND='1' and BOTON_PRES= x"E" then segm<= x"E";
        elsif IND='1' and BOTON_PRES= x"F" then segm<= x"F";
        else 
        segm <= segm; 
     end if; 
   end if;  
end process; 

--SALIDAS
FLAG <= IND; 
Sout <= segm; 


end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP is
  Port ( 
    clk 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	columns   : IN  STD_LOGIC_VECTOR(2  DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	rst : in std_logic;  
	rows : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	lcd_e  : out std_logic;
    lcd_rs : out std_logic;
    lcd_rw : out std_logic;
    wave: out std_logic; 
    lcd_db : out std_logic_vector(7 downto 4);
    S_motor: out std_logic_vector(3 downto 0);
    rd_uart, wr_uart: in std_logic; -- Para el FIFO
	rx: in std_logic;
	w_data: in std_logic_vector(7 downto 0);
	tx_full, rx_empty: out std_logic;
	tx: out std_logic
	);
end TOP;

architecture Behavioral of TOP is

signal key_out: std_logic_vector(7 downto 0);
signal key_flag: std_logic := '0';
signal flag: std_logic := '0';
signal data_io: std_logic_vector(3 downto 0) := (others => '0');
signal start_motor: std_logic; 
signal dir_motor: std_logic;
signal en_sonido: std_logic;
 

begin

KEYPAD : entity work.KEYPAD(Behavioral)
port map (
    clk => clk,
    COL => columns,
    ROW => rows,
    Sout => data_io, 
    FLAG => flag
);

LCD: entity work.LOGIC_LCD
port map(
    clk => clk,
    lcd_e =>lcd_e,
    lcd_rs => lcd_rs,
    lcd_rw => lcd_rw,
    lcd_db => lcd_db,
    data_entry => key_out,
    dir => dir_motor,
    start => start_motor,
    en_buzzer => en_sonido,
    data_flag => flag
);

BUZZER: entity work.buzzer
port map(
        clk => clk,
        enable => en_sonido,
        wave => wave
);


UART_MOTOR: entity work.uart_motor
port map (
    clk => clk,
    rst => rst,
    start => start_motor,
    dir => dir_motor,
    rd_uart => rd_uart,
    wr_uart => wr_uart,
	rx => rx,
	w_data => w_data,
	tx_full => tx_full,
	rx_empty => rx_empty,
	tx => tx,
    S => S_motor
); 




process(clk,data_io)
begin
case data_io is
when x"1" => key_out <= x"31";
when x"2" => key_out <= x"32";
when x"3" => key_out <= x"33";
when x"4" => key_out <= x"34";
when x"5" => key_out <= x"35";
when x"6" => key_out <= x"36";
when x"7" => key_out <= x"37";
when x"8" => key_out <= x"38";
when x"9" => key_out <= x"39";
when x"E" => key_out <= x"2A";
when x"F" => key_out <= x"23";
when x"0" => key_out <= x"30";
when others => key_out <= x"58";
end case;
end process;


end Behavioral;

`timescale 1ns / 1ps
module word_memory(
    input clk,rst,
    output reg [39:0] word
    );

localparam [39:0] PIOJO = 40'h50494F4A4F;
localparam [39:0] INDIO = 40'h494e44494f;
localparam [39:0] GALLO = 40'h47414c4c4f;
localparam [39:0] BALON = 40'h42414c4f4e;
localparam [39:0] LECHE = 40'h4c45434845;
localparam [39:0] NOCHE = 40'h4E4F434845;
localparam [39:0] LLAMA = 40'h4C4C414D41;
localparam [39:0] ANGEL = 40'h414e47454c;
localparam [39:0] ABEJA = 40'h4142454a41;
localparam [39:0] KOALA = 40'h4b4f414c41;

reg [3:0] cont = 0;

always @(posedge clk or posedge  rst) begin
    if(rst) cont <= 0;
    else if(cont > 4'd8) cont <= 4'd0;
    else cont <= cont + 4'd1;
end

always@(cont)
   case (cont)
      4'd0: begin
                  word <= PIOJO;
               end
      4'd1: begin
                  word <= INDIO;
               end
      4'd2: begin
                  word <= GALLO;
               end
      4'd3: begin
                 word <= BALON;
               end
      4'd4: begin
                  word <= LECHE;
               end
      4'd5: begin
                  word <= NOCHE;
               end
      4'd6: begin
                  word <= LLAMA;
               end
      4'd7: begin
                  word <= ANGEL;
               end
      4'd8: begin
                  word <= ABEJA;
               end
      4'd9: begin
                  word <= KOALA;
               end
      default: begin
               word <= PIOJO;
      end
   endcase   

        

endmodule
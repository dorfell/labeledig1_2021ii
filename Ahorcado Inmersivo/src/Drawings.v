`timescale 1ns / 1ps
module vga_memory(
    input [1:0] drawing,
    input [3:0] address,
    output reg [15:0] data
    );

localparam [255:0] hangman = 256'h3FF83FF8332036703C503870302030F83020307030503050FC00FC00;
localparam [255:0] pacman   = 256'h3E00FF81EFC1FFC3FF03F803C003F803FF01FFC1FFC0FF803E00000;
localparam [255:0] happy   = 256'h7E00810300C200440028811800180018FF1981988114C3227E4300C081007E0;
localparam [255:0] ghost   = 256'h00003C00FF01FF83FFC3FFC399C799E7FFE7FFE6666599A7FFE6E7646620000;

  
always @(drawing,address)
   case (drawing)
      2'b00 : begin
                  data <= hangman[(255-address*16)-:16];
               end
      2'b01 : begin
                  data <= pacman[(255-address*16)-:16];
               end
      2'b10 : begin
                  data <= happy[(255-address*16)-:16];
               end
      2'b11 : begin
                  data <= ghost[(255-address*16)-:16];
               end
      default: begin
                  data <= data;
               end
   endcase
    
    
    
endmodule

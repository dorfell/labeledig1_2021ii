
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vga_drawer is
  Port (
    -- In ports
    visible      : in std_logic;
    col          : in unsigned(9 downto 0);
    fila         : in unsigned(9 downto 0);
    dato_memo    : in std_logic_vector(15 downto 0);
    addr_memo    : out std_logic_vector(3 downto 0);
    -- Out ports
    rojo         : out std_logic_vector(3 downto 0);
    verde        : out std_logic_vector(3 downto 0);
    azul         : out std_logic_vector(3 downto 0)
  );
end vga_drawer;

architecture behavioral of vga_drawer is
  constant MAX_COLUMNA: positive := 256;
  constant MAX_FILA : positive := 256;
  -- pixel en blanco/negro que viene de la memoria. Por tanto es de un unico bit
  signal pixel_memo : std_logic;

begin

  -- la memoria tiene 4 bits de direcciones, ya que tiene 16 filas:
  -- se toman los 4 bits menos significativos de la fila
  -- como fila es unsigned y addr_memo es std_logic_vector, hay que hacer cast
  addr_memo <= std_logic_vector(fila(7 downto 4));


  -- seleccionamos el pixel que viene de la memoria
  -- la imagen tiene 16 columnas, con los 4 bits menos significativos
  -- de la columna se selecciona el bit correspondiente.
  -- Esto es un multiplexor, que estan explicados en el manual:
  --   http://hdl.handle.net/10115/4045
  -- y en estos videos:
  --   https://youtu.be/CQdw-M1ooTI
  --   https://youtu.be/or_vwCTSY8M
  -- lineas de codigo, aunque seguramente esta se entienda mejor, y son 
  -- equivalentes en cuanto al circuito generado
  -- la imagen va a salir en espejo horizontal respecto a la memoria
  with col(7 downto 4) select
    pixel_memo <= dato_memo(15)  when "0000",
                  dato_memo(14)  when "0001",
                  dato_memo(13)  when "0010",
                  dato_memo(12)  when "0011",
                  dato_memo(11)  when "0100",
                  dato_memo(10)  when "0101",
                  dato_memo(9)  when "0110",
                  dato_memo(8)  when "0111",
                  dato_memo(7)  when "1000",
                  dato_memo(6)  when "1001",
                  dato_memo(5) when "1010",
                  dato_memo(4) when "1011",
                  dato_memo(3) when "1100",
                  dato_memo(2) when "1101",
                  dato_memo(1) when "1110",
                  dato_memo(0) when others; -- "0011",

  -- alternativa mas corta para describir el mismo multiplexor anterior
  -- con una sola linea. Aunque seguramente la anterior se entienda mejor,
  -- son equivalentes en cuanto al circuito generado
  -- como la columna es un std_logic_vector, hay que pasarlo a entero
  --pixel_memo <= dato_memo(to_integer(col(3 downto 0)));


  -- pintamos el pixel blanco o negro segun el color del pixel
  -- se podria utilizar una paleta y pintar de otros dos colores
  P_display: Process (visible, pixel_memo)
  begin
    -- cuando no es visible se ponen todos los colores a negro
    if(visible = '0' or col > MAX_COLUMNA or fila > MAX_FILA) then 
        rojo   <= (others => '0');
        verde  <= (others => '0');
        azul   <= (others => '0');
    else 
      rojo<= (others => pixel_memo);
      verde <=  (others => pixel_memo);
      azul  <= (others => pixel_memo); 
    end if;
  end process;
  
end Behavioral;


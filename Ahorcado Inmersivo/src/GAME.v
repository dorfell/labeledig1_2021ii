`timescale 1ns / 1ps

module GAME(
input clk, rst,

output lcd_e,lcd_rs,lcd_rw,
output [7:4] lcd_db,

input[3:0] columns,
output[3:0] rows,

output hsync, vsync,
output [3:0] red,blue,green,

output servo

);

parameter [27:0] t_HOME_TEXT_DELAY 	= 250000000;// //3s 		== ~1clk
parameter [27:0] t_LCD_INIT 	    = 2500000;//20 ms 	
parameter [27:0] t_AFTER_HOME 	    = 1;//2ns 	
parameter [27:0] t_servo            = 125000000; // 100 ms


//===============================================================================================
//-----------------------------Create the counting mechanisms------------------------------------
//===============================================================================================
reg [27:0] cnt_timer=0; 			//Used as a counter to delay the state machine
reg flag_HOME_TEXT_DELAY=0,flag_LCD_INIT =0, flag_AFTER_HOME =0, flag_SERVO=0;
reg flag_rst=1;					//Start with flag RST set. so that the counting has not started

always @(posedge clk) begin
	if(flag_rst) begin
		flag_HOME_TEXT_DELAY	<=	1'b0;		//Unlatch the flag
		flag_LCD_INIT 	<=	1'b0;	
		flag_AFTER_HOME 	<=	1'b0;		//Unlatch the flag
		cnt_timer	<=	20'b0;	
		flag_SERVO <= 1'b0;
	end
	else begin
		if(cnt_timer>=t_HOME_TEXT_DELAY) begin			
			flag_HOME_TEXT_DELAY	<=	1'b1;
		end
		else begin			
			flag_HOME_TEXT_DELAY <=	flag_HOME_TEXT_DELAY;
		end
		//----------------------------
		if(cnt_timer>=t_LCD_INIT ) begin			
			flag_LCD_INIT 	<=	1'b1;
		end
		else begin			
			flag_LCD_INIT 	<=	flag_LCD_INIT ;
		end
		//----------------------------
		if(cnt_timer>=t_AFTER_HOME ) begin			
			flag_AFTER_HOME	<=	1'b1;
		end
		else begin			
			flag_AFTER_HOME <= flag_AFTER_HOME;
		end
		//----------------------------
		if(cnt_timer>=t_servo ) begin			
			flag_SERVO	<=	1'b1;
		end
		else begin			
			flag_SERVO <= flag_SERVO;
		end
		cnt_timer	<= cnt_timer + 1;
	end
end

//===============================================================================================
//----------------------------- State Machine Declaration ------------------------------------
//===============================================================================================

reg [6:0] present_state, next_state;	 
parameter s0=0, s1=1, s2=2, s3=3, s4=4,s5=5,s6=6, s7=7;


//===============================================================================================
//----------------------------- LCD Declaration ------------------------------------
//===============================================================================================

reg [127:0]line1_buffer;
reg [127:0]line2_buffer; 

lcd16x2_ctrl lcd_controller(clk, rst,lcd_e,lcd_rs,lcd_rw,lcd_db,line1_buffer,line2_buffer);

//===============================================================================================
//----------------------------- Keyboard Declaration ------------------------------------
//===============================================================================================

wire [3:0] button_pressed; //The button pressed in the keyboard
wire button_pressed_ind; //Indicates when a button has been pressed 
wire [7:0] character_pressed; //The characther corresponding to a certain button in the keyboard

LOGIC_KEYPAD keypad(clk,columns,rows,button_pressed,button_pressed_ind);
KEYPAD_DECODER keypad_decoder(button_pressed,character_pressed); 


//===============================================================================================
//----------------------------- VGA Declaration ------------------------------------
//===============================================================================================

reg [2:0] drawing;

vga_top vga(clk,rst,drawing,hsync,vsync,red,blue,green);


//===============================================================================================
//----------------------------- Memory of Words ------------------------------------
//===============================================================================================

wire [39:0] word_in_memory;

word_memory word_memory(clk,rst,word_in_memory);

//wire word_check_enable = (present_state == s4);

reg[39:0] word_template = 40'h4141414141;
reg[39:0] user_word = 40'h5F5F5F5F5F;
//wire[39:0] word_check_result;
//word_check word_check( word_check_enable,character_pressed,word_template,line1_buffer[127:88],word_check_result);

//===============================================================================================
//----------------------------- Servo motor ------------------------------------
//===============================================================================================

reg[6:0] pos = 7'b0;
Servotop servom(clk,reset,pos,servo);




//===================
// Game (State 3)
//===================

reg[2:0] tries = 0;

//===================
// Next State Logic
//===================
always @(posedge clk or posedge rst) begin
  if (rst) begin
    present_state <= s0;
    flag_rst <= 1;
  end 
  else begin
  
  flag_rst <= 0;
  present_state <= next_state;
  
  case (present_state)
    s0:
        if(flag_LCD_INIT) begin
            next_state <= s1;   
            flag_rst <= 1;
        end else
            flag_rst <= 0;
    s1:
     if(button_pressed_ind)begin
      next_state <= s3;    
      flag_rst <= 1; 
     end
     else if(flag_HOME_TEXT_DELAY)begin
      next_state <= s2;
      flag_rst <= 1;
     end  else 
      flag_rst <= 0;
    s2:
     if(button_pressed_ind)begin
      next_state <= s3;
      flag_rst <= 1; 
     end
     else if(flag_HOME_TEXT_DELAY)begin
      next_state <= s1;
      flag_rst <= 1; 
     end
     else 
      flag_rst <= 0;
    s3:
    if(flag_AFTER_HOME)begin
      next_state <= s4;
      flag_rst <= 1; 
     end
     else 
      flag_rst <= 0; 
    s4:
      if(user_word == word_template) next_state <= s6;
      else if(tries == 3'd14) next_state <= s5;   
    s5:
        ;
    s6:
      if(flag_SERVO)begin
       next_state <= s7;
       flag_rst <= 1;
       end 
       else 
      flag_rst <= 0; 
    s7:
      if(flag_SERVO)begin
       next_state <= s6;
       flag_rst <= 1;
       end 
       else 
      flag_rst <= 0; 
    default:
      next_state <= s0;
    endcase 
 end   
 end

//===================	
// Output logic
//===================	 
always @ (present_state,button_pressed_ind) begin
    case (present_state)
    s0: begin
      line1_buffer <= 128'h20202020202020202020202020202020;
      line2_buffer <= 128'h20202020202020202020202020202020;
      drawing <= 0;
    end
    s1: begin
      line1_buffer <= 128'h57454C434F4D4520544F202020202020;
      line2_buffer <= 128'h48414E474D414E2047414D4520202020;
      drawing <= 0;
    end   
    s2: begin
      line1_buffer <= 128'h505245535320414E59204B4559202020;  
      line2_buffer <= 128'h544F20434F4E54494E55452020202020;
      drawing <= 0;
    end
    s3: begin  
        word_template <= word_in_memory;
        line1_buffer <= 128'h5F5F5F5F5F2020202020202020202020;
        line2_buffer <= 128'h20202020202020202020202020202020;
    end
    s4: begin 
    drawing <= 2'b01;
   
    
        if( button_pressed_ind == 1) begin
           
            user_word[39:32] <= character_pressed == word_template[39:32] ? character_pressed : user_word[39:32];
            user_word[31:24] <= character_pressed == word_template[31:24] ? character_pressed : user_word[31:24];
            user_word[23:16] <= character_pressed == word_template[23:16] ? character_pressed : user_word[23:16];
            user_word[15:8]  <= character_pressed == word_template[15:8]  ? character_pressed : user_word[15:8] ;
            user_word[7:0]   <= character_pressed == word_template[7:0]   ? character_pressed : user_word[7:0]  ;
            
               
        end
        
        line1_buffer <= {user_word,88'h2020202020202020202020};
        line2_buffer <= {character_pressed,8'h20,word_template,72'h202020202020202020};
    end
    s5: begin
        line1_buffer <= 128'h594F55204C4F53452020202020202020;
        line2_buffer <= 128'h20202020202020202020202020202020;
        drawing <= 2'b11;               
    end
    s6: begin
        line1_buffer <= 128'h594F552057494E202020202020202020;
        line2_buffer <= 128'h20202020202020202020202020202020;
        drawing <= 2'b10;        
        pos <= 7'b1011010;
    end
     s7: begin
        line1_buffer <= 128'h594F552057494E202020202020202020;
        line2_buffer <= 128'h20202020202020202020202020202020;
        drawing <= 2'b10;        
        pos <= 7'b0000000;
    end
  endcase
end
endmodule

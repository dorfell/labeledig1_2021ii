----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.01.2022 12:24:41
-- Design Name: 
-- Module Name: KEYPAD_DECODER - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity KEYPAD_DECODER is
PORT(
	BOTON_PRES : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);				   
	CHAR : OUT	STD_LOGIC_VECTOR(7 DOWNTO 0)					  
);
end KEYPAD_DECODER;



architecture Behavioral of KEYPAD_DECODER is
begin
process(BOTON_PRES)
begin
    case (BOTON_PRES) is
      when "0000" =>
         CHAR <= x"4E"; -- N
      when "0001" =>
         CHAR <= x"41"; -- A
      when "0010" =>
         CHAR <= x"42"; -- B
      when "0011" =>
         CHAR <= x"43"; -- C
      when "0100" =>
         CHAR <= x"45"; -- E
      when "0101" =>
         CHAR <= x"46"; -- F
      when "0110" =>
         CHAR <= x"47"; -- G
      when "0111" =>
         CHAR <= x"49"; -- I
      when "1000" =>
         CHAR <= x"4A"; -- J
      when "1001" =>
        CHAR <= x"4B"; -- K
      when "1010" =>
         CHAR <= x"44"; -- D
      when "1011" =>
         CHAR <= x"48"; -- I
      when "1100" =>
         CHAR <= x"4C"; -- L
      when "1101" =>
         CHAR <= x"50"; -- P
      when "1110" =>
         CHAR <= x"4D"; -- M
      when "1111" =>
         CHAR <= x"4F"; -- O
      when others =>
         CHAR <= x"58";
   end case;
end process;

end Behavioral;

`timescale 1ns / 1ps


module vga_top(
    input clk, reset,
    input [2:0] drawing,
    output hsync, vsync,
    output [3:0] red,blue,green
    );
    
    wire  video_on, pixel_tick;
    wire [3:0] address_memory;
    wire [15:0] data_memory;
    wire[9:0] fila, columna;
    
    vga_drawer drawer(video_on,columna,fila,data_memory,address_memory,red,green,blue);
    vga_sync controller(clk,reset,hsync,vsync,video_on,pixel_tick,columna,fila);
    vga_memory memory(drawing,address_memory,data_memory);
    
    
endmodule

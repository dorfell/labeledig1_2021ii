----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.01.2022 23:20:12
-- Design Name: 
-- Module Name: servo_pwm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity servo_pwm is
  Port ( clk, rst: in std_logic;
        pos1   : IN  STD_LOGIC_VECTOR(6 downto 0);
        servo : OUT STD_LOGIC );
end servo_pwm;

architecture Behavioral of servo_pwm is

-- Contador de 0 a 1279.
    signal cnt : unsigned(11 downto 0);
    signal pos_comp : STD_LOGIC_VECTOR(6 downto 0);
    -- Se�al temporal para generar el PWM.
    signal pwmi: unsigned(7 downto 0);
begin
 -- comparador
    pos_comp <= "1011010" when unsigned(pos1) > "1011010" else pos1; 
    
    -- Valor m�nimo 0 debe ser de 1ms  
    pwmi <= unsigned('0' & pos_comp) + 90;
    -- Proceso del contador, de 0 a 1279.
    contador: process (rst, clk) begin
        if (rst = '1') then
            cnt <= (others => '0');
        elsif rising_edge(clk) then
            if (cnt = 1799) then 
                cnt <= (others => '0');
            else
                cnt <= cnt + 1;
            end if;
        end if;
    end process;
    -- Se�al de salida para el servomotor.
    servo <= '1' when (cnt < pwmi) else '0';
end Behavioral;

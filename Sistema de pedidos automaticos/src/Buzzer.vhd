library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Buzzer is
    	port ( clk, rst :  in std_logic;
    	       Ent_Buzzer: in std_logic_vector (7 downto 0);
               Buzzer  : out std_logic
               );
end Buzzer;

architecture Behavioral of Buzzer is
signal ee, qq : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf   : std_logic;
signal duty_cycle : std_logic_vector(27 downto 0):= x"0000000";
signal en1: std_logic := '0'; 

begin


mux_add: 
  ee <= qq + 1  when ovf='0' else
        (others=>'0'); 
ovf_com:				    
  ovf <= '1' when qq = x"0000172" else 
  '0';

mux_cycle:
    duty_cycle <= x"0000172";
    
en_comp :
    en1 <= '0' when qq < duty_cycle else
           '1';
                  
                  
process(clk, rst, Ent_Buzzer)
begin
if rst = '1' then
qq <=(others =>'0');
if (Ent_Buzzer="01101101" or Ent_Buzzer="01100001") then
  if (clk'event and clk='1') then -- Hubo un flanco ascendent
    qq <= ee;
  else
    qq <= qq;
  end if;
  end if;
  end if;	
end process;

Buzzer <= en1;

end Behavioral;
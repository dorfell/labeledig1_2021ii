library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity logica_usuario is
  port (
    clk    : in  std_logic;
    lcd_e  : out std_logic;
    lcd_rs : out std_logic;
    lcd_rw : out std_logic;
    lcd_db : out std_logic_vector(7 downto 4);
    data_entry : in std_logic_vector(7 downto 0); --DATOS DE ENTRADA
    flag: in std_logic); --INDICA QUE SE PULSA UN BOTON

end entity logica_usuario;

-------------------------------------------------------------------------------

architecture behavior of logica_usuario is

  -- 
  signal timer : natural range 0 to 100000000 := 0;
  signal switch_lines : std_logic := '0';
  signal line1 : std_logic_vector(127 downto 0); --16 POSICIONES, CADA UNA DE 8 BITS
  signal line2 : std_logic_vector(127 downto 0); --16 POSICIONES, CADA UNA DE 8 BITS
  signal count : integer range 0 to 10:= 0;

  -- component generics
  constant CLK_PERIOD_NS : positive := 8;  -- 125 Mhz

  -- component ports
  signal rst          : std_logic;
  signal line1_buffer : std_logic_vector(127 downto 0) := (others => '0');--SE�ALES: TRANSPORTAN EL MENSAJE
  signal line2_buffer : std_logic_vector(127 downto 0) := x"3E202020202020202020202020202020";

  -- Componentes m�quina de estados
  type states is (s0,s1,s2,s3,s4,s5);
  signal present_state, next_state: states;
      
begin  -- architecture behavior

  -- component instantiation
  DUT : entity work.controlador 
    generic map (
      CLK_PERIOD_NS => CLK_PERIOD_NS)
    port map (
      clk          => clk,
      rst          => rst,
      lcd_e        => lcd_e,
      lcd_rs       => lcd_rs,
      lcd_rw       => lcd_rw,
      lcd_db       => lcd_db,
      line1_buffer => line1_buffer,
      line2_buffer => line2_buffer);

  rst <= '0';

  -- see the display's datasheet for the character map
  
--===================
-- Present State
--===================
process(clk, flag)
  begin
    present_state <= s0;
  if ((clk'event and clk='1') and flag='1') then
	   present_state <= next_state;
    else		
	   present_state <= present_state;
  end if;
end process;

--===================
-- Next state logic
--===================
process(present_state, data_entry)
  begin 
  case present_state is 
    when s0 =>
	   if (data_entry = x"30") then
		   next_state <= s1;
		else 
		   next_state <= s0;
		end if;

    when s1 =>
	    if (data_entry = x"30") then
		   next_state <= s2;
		elsif (data_entry = x"31" or data_entry = x"32" or data_entry = x"33") then
		   next_state <= s3;
		else 
		   next_state <= s1;
		end if;

    when s2 =>
		if (data_entry = x"31" or data_entry = x"32" or data_entry = x"33") then
		   next_state <= s3;
		elsif (data_entry = x"2A") then
		   next_state <= s1;
		else 
		   next_state <= s2;
		end if;

    when s3 =>
	    if (data_entry = x"30") then
		   next_state <= s4;
		elsif (data_entry = x"31" or data_entry = x"32" or data_entry = x"33") then
		   next_state <= s5;
		else 
		   next_state <= s3;
		end if;

    when s4 =>
		if (data_entry = x"31" or data_entry = x"32" or data_entry = x"33") then
		   next_state <= s5;
		elsif (data_entry = x"2A") then
		   next_state <= s3;
		else 
		   next_state <= s4;
		end if;
	      
	when s5 =>
		if (data_entry = x"30") then
		   next_state <= s0;
		else 
		   next_state <= s5;
		end if;
					
  end case;
end process;

--===================	
-- Output logic
--===================
process(present_state, clk, line1, line2)
  begin
  case present_state is
    when s0 =>   
    --MENSAJE PREDETERMINADO: Bienvenid@s al
    line1 <= x"4269656E76656E6964407320616C2020";
    line1_buffer <= line1;
    -- Restaurante GL
    line2 <= x"52657374617572616E746520474C2020";
    line2_buffer <= line2;
    
    when s1 =>
	--MENSAJE PREDETERMINADO: Menu principal:
    line1 <= x"4D656E75207072696E636970616C3A20";
    line1_buffer <= line1;
    -- 1.Pizza
    line2 <= x"312E50697A7A61202020202020202020";
    line2_buffer <= line2;   
   
    when s2 =>
	--MENSAJE PREDETERMINADO: 2.Hamburguesa
    line1 <= x"322E48616D6275726775657361202020";
    line1_buffer <= line1;
    -- 3.Perro
    line2 <= x"332E506572726F202020202020202020";
    line2_buffer <= line2; 
  
    when s3 =>
	--MENSAJE PREDETERMINADO: Menu bebidas:
    line1 <= x"4D656E7520626562696461733A202020";
    line1_buffer <= line1;
    -- 3.Perro
    line2 <= x"312E476173656F736120202020202020";
    line2_buffer <= line2; 
	
    when s4 =>
	--MENSAJE PREDETERMINADO: 2.Jugo
    line1 <= x"322E4A75676F20202020202020202020";
    line1_buffer <= line1;
    -- 3.Agua
    line2 <= x"332E4167756120202020202020202020";
    line2_buffer <= line2;
    
    when s5 =>
	--MENSAJE PREDETERMINADO: Su pedido se
    line1 <= x"53752070656469646F20736520202020";
    line1_buffer <= line1;
    -- esta preparando
    line2 <= x"6573746120707265706172616E646F20";
    line2_buffer <= line2;

  end case;
end process;

end architecture behavior;
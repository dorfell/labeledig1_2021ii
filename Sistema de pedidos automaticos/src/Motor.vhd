library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Motor is
Port ( clk : in std_logic;
       rst: in std_logic;
       Ent_motor: in std_logic_vector(7 downto 0);
       S: out std_logic_vector(3 downto 0)
      );
end Motor;

architecture Behavioral of Motor is
signal ien1 : std_logic;

-- Component declaration for the Unit Under Test (UUT)
component Divisor_Frecuencia
    port ( clk, rst :  in std_logic;
           en1  : out std_logic
          );
end component;

component FSM1
    Port (clk,rst,en: in std_logic;
          Ent_motor: in std_logic_vector(7 downto 0);
          S: out std_logic_vector(3 downto 0) );
end component;

begin
-- Instantiate the Unit Under Test UUT;

Instancia_Divisor_Frecuencia: Divisor_Frecuencia Port map(
    clk => clk,
    rst => rst,
    en1 => ien1
    );
    
Instancia_FSM1: FSM1 Port map(
    clk => clk,
    rst => rst,
    en => ien1,
    Ent_motor => Ent_motor,
    S(0) => S(0),
    S(1) => S(1),
    S(2) => S(2),
    S(3) => S(3)
    );

end Behavioral;
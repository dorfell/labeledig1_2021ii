library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PROYECTO_TOP is
  Port ( 
    clk, rst: in std_logic;
    columns: in  std_logic_vector(2 downto 0);
    rd_uart, wr_uart: in std_logic;
	rx: in std_logic;
	w_data: in std_logic_vector(7 downto 0);
	rows: out std_logic_vector(3 DOWNTO 0); 
	lcd_e  : out std_logic;
    lcd_rs : out std_logic;
    lcd_rw : out std_logic;
    lcd_db : out std_logic_vector(7 downto 4);
	tx_full, rx_empty: out std_logic;
	tx: out std_logic;
    S: out std_logic_vector(3 downto 0)
    );
end PROYECTO_TOP;

architecture Behavioral of PROYECTO_TOP is
begin

Inst_UART_Motor_Buzzer : entity work.UART_Motor_Buzzer
port map (
	clk => clk,
	rst => rst,
	rd_uart => rd_uart,
	wr_uart => wr_uart,
	rx => rx,
	w_data => w_data,
	tx_full => tx_full,
	rx_empty => rx_empty,
	tx => tx,
	S => S
);

Inst_KEY_LCD: entity work.KEY_LCD
port map ( 
    clk => clk,
    columns => columns,
    rows => rows,
    lcd_e =>lcd_e,
    lcd_rs => lcd_rs,
    lcd_rw => lcd_rw,
    lcd_db => lcd_db
);

end behavioral;
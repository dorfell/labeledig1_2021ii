library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity baud_gen is
     generic( M: integer := 407;   --it counts until M and generates a tick
              MBITS: integer := 9  --number of bits to get M, MBITS = Log2(M)
              );
     port (clk, rst: in std_logic;
           baud_ticks: out std_logic );
end baud_gen;

architecture Behavioral of baud_gen is

signal r_reg, r_next: unsigned( MBITS-1 downto 0);  

begin

--register 
    process (clk, rst)
    begin
        if (rst='1') then
           r_reg <=(others=>'0');
        elsif (clk'event and clk='1') then
           r_reg<=r_next;
        end if;
    end process;
    
    --logic
    r_next <= (others=> '0') when r_reg = (M-1) else r_reg + 1;   --overflow r_next after counting M clock signals    
    --out  
    
    baud_ticks <='1' when r_reg = (M-1) else '0'; -- it generates a tick of one-clock signal when it has counted M  clock signals
    

end Behavioral;
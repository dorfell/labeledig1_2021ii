library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity KEY_LCD is
  Port ( 
    clk 		  : IN  STD_LOGIC; 				   --RELOJ FPGA
	columns       : IN  STD_LOGIC_VECTOR(2  DOWNTO 0); --PUERTO CONECTADO A LAS COLUMNAS DEL TECLADO
	rows 	      : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);  --PUERTO CONECTADO A LA FILAS DEL TECLADO
	lcd_e  : OUT std_logic;
    lcd_rs : OUT std_logic;
    lcd_rw : OUT std_logic;
    lcd_db : OUT std_logic_vector(7 downto 4)
    );
end KEY_LCD;

architecture Behavioral of KEY_LCD is

signal key_out: std_logic_vector(7 downto 0);
signal key_flag: std_logic := '0';
signal flag: std_logic := '0';
signal data_io: std_logic_vector(3 downto 0) := (others => '0');

begin

keypad1 : entity work.keypad(Behavioral)
port map (
    CLK => clk,
    COL => columns, 
    ROW => rows,
    Sout => data_io, 
    flag => flag
);

lcd: entity work.logica_usuario
port map(
    clk => clk,
    lcd_e =>lcd_e,
    lcd_rs => lcd_rs,
    lcd_rw => lcd_rw,
    lcd_db => lcd_db,
    data_entry => key_out,
    flag => flag
);

process(clk,data_io)
begin
case data_io is
when x"0" => key_out <= x"30";
when x"1" => key_out <= x"31";
when x"2" => key_out <= x"32";
when x"3" => key_out <= x"33";
when x"4" => key_out <= x"34";
when x"5" => key_out <= x"35";
when x"6" => key_out <= x"36";
when x"7" => key_out <= x"37";
when x"8" => key_out <= x"38";
when x"9" => key_out <= x"39";
when x"E" => key_out <= x"2A"; --(*)
when x"F" => key_out <= x"23"; --(#)
when others => key_out <= x"58";
end case;
end process;


end behavioral;
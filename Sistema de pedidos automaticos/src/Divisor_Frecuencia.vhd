library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Divisor_Frecuencia is
    	port ( clk, rst :  in std_logic;
               en1  : out std_logic
               );
end Divisor_Frecuencia;

architecture Behavioral of Divisor_Frecuencia is
signal ee, qq : std_logic_vector(27 downto 0):=(others=>'0');
signal ovf   : std_logic;
signal duty_cycle : std_logic_vector(27 downto 0):= x"0000000";

begin

mux_add: 
  ee <= qq + 1  when ovf='0' else
        (others=>'0'); 
ovf_com:				    
  ovf <= '1' when qq = x"004C4B4" else 
  '0';

mux_cycle:
    duty_cycle <= x"004C4B4";
    
en_comp :
    en1 <= '0' when qq < duty_cycle else
           '1';
                  
                  
process(clk,rst)
begin
if rst = '1' then
qq <=(others =>'0');
  elsif clk'event and clk='1' then -- Hubo un flanco ascendent
    qq <= ee;
  else
    qq <= qq;
  end if;	
end process;

end Behavioral;
# labeledig1_2021II

Projects developed in the digital electronic lab. Hardware Platform: Zybo-Z7. Tools: Vivado. Period: 2021-II.

## Ahorcado Inmersivo
- Ana Quintero: aquinterol@unal.edu.co
- Juan Diaz:    jdiazga@unal.edu.co

## Casilleros Seguros SEGT
- Eduardo Toro:   etorob@unal.edu.co
- Sergio Guevara: seguevarac@unal.edu.co
- https://youtu.be/o3saMcXIEBY

## Dispensador de alimento Feedy
- Diana Cortés:  dcortesn@unal.edu.co
- Felipe Torres: jutorresma@unal.edu.co
- https://www.youtube.com/watch?v=_8AIZ33sIvo

## Dispensador de medicamento
- Erick Cepeda: ercepeda@unal.edu.co
- Hans Peralta: hperalta@unal.edu.co

## Restaurante Inteligente
- Harrison Pineda: hangelp@unal.edu.co
- Santiago Cadena: scadenaa@unal.edu.co

## Sistema de seguridad electrónico
- Jorge Hernandez: jhernandezga@unal.edu.co
- Raúl Guzmán:     raguzmanma@unal.edu.co
- https://youtu.be/Q1lhBqDQsvU


## Sistema de pedidos automático
- Gaia Braccia: gbraccia@unal.edu.co
- Laura Solano: lasolanog@unal.edu.co


